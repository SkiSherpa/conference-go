from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Conference, Location, State
from common.json import ModelEncoder
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from .acls import get_city_pic



class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

    """
    Lists the conference names and the link to the conference.
    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
    else:
        content = json.loads(request.body)
        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conferences = Conference.objects.create(**content)
    return JsonResponse(
        {"conferences": conferences},
        encoder=ConferenceListEncoder,
    )

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

def api_show_conference(request, id):
    """
    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

    """
    Lists the location names and the link to the location.
    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
    else:
        content = json.loads(request.body)

        # PEXEL fn goes here
        city_pic = get_city_pic(content["city"])
        content.update(city_pic)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        # you should be able to pass a state to acls, as long as city_pic & content.update are called after the try/except

        locations = Location.objects.create(**content)

    return JsonResponse(
        {"locations": locations},
        encoder=LocationListEncoder,
        safe=False,
    )


    """
    Returns the details for the Location model specified
    by the id parameter.
    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # convert the submitted JSON-formated str into a dict
        content = json.loads(request.body)
        # convert the state abbreviation into a State, if it exists
        try:
            if "state" in content:
                # converting the state abbreviation?
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        # Use that dictionary to update the existing Location.
        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
    # Return the updated Location object.
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )
