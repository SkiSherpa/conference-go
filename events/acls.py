from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

# use Pexels Search for Photos API to get the url of city pic
# save as a part of Location instance -
def get_city_pic(city):
    url = f'https://api.pexels.com/v1/search'
    headers = {"Authorization": PEXELS_API_KEY}
    parameters = {
        "query": city,
    }
    response = requests.get(url, params=parameters, headers=headers)
    pic = json.loads(response.content)
    city_pic = {
          "picture_url": pic["photos"][0]["url"],
    }
    return city_pic
# return only dicts

# def weather_details():
#         url = f'http://api.openweathermap.org/geo/1.0/direct?q={content.city},{content.state.abbreviation},{"US"}&limit={1}&appid={PEXELS_API_KEY}'
#         headers = {"Authorization": PEXELS_API_KEY}
#         responese = requests.get(url, headers=headers)
